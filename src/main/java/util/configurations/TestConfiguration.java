package util.configurations;

import util.enums.BrowserType;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TestConfiguration extends Properties {

	private static TestConfiguration properties;

	public static BrowserType browserParameter;
	public static String pageDefaultUrl;
	public static String pageZad1;
	public static String[] pageDefaultUser;
	public static String[] dbDefaultUser;
	public static String dbDefaultServer;
	public static String dbDefaultDatabase;

	public static void setProperties() {
		InputStream is = null;
		properties = new TestConfiguration();

		try {
			is = new FileInputStream("src/main/resources/properties.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		try {
			properties.load(is);
		} catch (IOException e) {
			e.printStackTrace();
		}

		init();
	}

	private static void init() {
		browserParameter = BrowserType.valueOf(properties.getProperty("browserParameter"));
		pageDefaultUrl = properties.getProperty("pageDefaultUrl");
		pageZad1=properties.getProperty("pageZad1");
		pageDefaultUser = properties.getProperty("pageDefaultUser").split(",");
		dbDefaultUser = properties.getProperty("dbDefaultUser").split(",");
		dbDefaultServer = properties.getProperty("dbDefaultServer");
		dbDefaultDatabase = properties.getProperty("dbDefaultDatabase");
	}

}
