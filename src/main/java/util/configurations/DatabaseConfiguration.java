package util.configurations;

import com.sun.jna.platform.win32.Guid;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;

//NOTE:
// 1. methods not tested under Java solution
// 2. methods prepared for MS SQL driver: com.microsoft.sqlserver.jdbc.SQLServerDriver
public class DatabaseConfiguration {
	private String connectionUrl;
	private Connection myConnection;

	private String dbDefaultUser;
	private String dbDefaultPassword;
	private String dbDefaultServer;
	private String dbDefaultDatabase;

	public DatabaseConfiguration() {
		dbDefaultUser = TestConfiguration.dbDefaultUser[0];
		dbDefaultPassword = TestConfiguration.dbDefaultUser[1];
		dbDefaultServer = TestConfiguration.dbDefaultServer;
		dbDefaultDatabase = TestConfiguration.dbDefaultDatabase;

		// Example:
		// connectionUrl = "jdbc:sqlserver://localhost:1433;" + "databaseName=AdventureWorks;user=UserName;password=*****"
		connectionUrl = "jdbc:sqlserver://" + dbDefaultServer + ";"
				+ "user=" + dbDefaultUser + ";"
				+ "password=" + dbDefaultPassword + ";"
				+ "database=" + dbDefaultDatabase;
	}

	// open connection to DB
	public void open() {
		try {
			myConnection = DriverManager.getConnection(connectionUrl);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// close connection to DB
	public void close() {
		try {
			myConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// Example SELECT query
	public Guid selectRandomGuid() {
		Statement stmt = null;
		ResultSet rs = null;

		try {
			stmt = myConnection.createStatement();
			rs = stmt.executeQuery("SELECT TOP 1 ClientGuid FROM Client WHERE ClientGuid <> '1' ORDER BY NEWID()");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (rs != null) try {
			rs.close();
		} catch (Exception e) {
		}
		if (stmt != null) try {
			stmt.close();
		} catch (Exception e) {
		}

		return (Guid) rs;
	}

	// Example UPDATE query
	public void updateOrderItem() {
		Statement stmt = null;
		ResultSet rs = null;

		try {
			stmt = myConnection.createStatement();
			rs = stmt.executeQuery("UPDATE OrderItem SET CreditsLeft = 0");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (rs != null) try {
			rs.close();
		} catch (Exception e) {
		}
		if (stmt != null) try {
			stmt.close();
		} catch (Exception e) {
		}
	}

	// Example DELETE query
	public void deleteClientLink() {
		Statement stmt = null;
		ResultSet rs = null;

		try {
			stmt = myConnection.createStatement();
			rs = stmt.executeQuery("DELETE FROM ClientLink WHERE ClientId <> 23");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (rs != null) try {
			rs.close();
		} catch (Exception e) {
		}
		if (stmt != null) try {
			stmt.close();
		} catch (Exception e) {
		}
	}

	// Example INSERT query
	public void insertOrder(String orderName) {
		Statement stmt = null;
		ResultSet rs = null;
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		java.util.Date now = new Date();
		String dateNow = sdfDate.format(now);

		try {
			stmt = myConnection.createStatement();
			rs = stmt.executeQuery("INSERT INTO [Order] VALUES(" + orderName + ", '" + dateNow + "')");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (rs != null) try {
			rs.close();
		} catch (Exception e) {
		}
		if (stmt != null) try {
			stmt.close();
		} catch (Exception e) {
		}
	}
}
