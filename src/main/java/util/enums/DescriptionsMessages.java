package util.enums;

/**
 * Created by ttutka on 10/14/2016.
 */
public enum DescriptionsMessages {

    noFound("No results found"),
    saveData("Twoje dane zostały poprawnie zapisane"),
    invalidFileFormat("Zły format pliku!"),
    invalidEmail("Nieprawidłowy email"),
    invalidPhoneNumber("Zły format telefonu - prawidłowy: 600-100-200"),
    messageSent("Wiadomość została wysłana"),
    invalidLogin("Nieprawidłowe dane logowania"),
    tooMuchQuantityProducts("Łączna ilość produktów w koszyku nie może przekroczyć 100."),
    orderPaid("Zamówienie opłacone"),
    validityCard("Upłynął termin ważności karty"),
    incorrectCvvFormat("Nieprawidłowy format CVV.");

    private final String message;

    private DescriptionsMessages(String s) {

        message = s;
    }

    public String toString() {

        return this.message;
    }
}
