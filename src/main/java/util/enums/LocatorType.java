package util.enums;

public enum LocatorType {
	ID,
	NAME,
	XPATH,
	CSS,
	CLASS
}
