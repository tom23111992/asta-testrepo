package util.objects;

import lombok.Getter;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ttutka on 7/19/2016.
 */

public class Basket {

    @Getter
    @Setter
    private List<BasketItem> items;
    @Getter
    @Setter
    private String totalQuantity;
    @Getter
    @Setter
    private String totalPrice;

    public Basket() {
        this.items = new LinkedList<>();
    }

}
