package util.objects;

import java.util.Dictionary;
import java.util.Hashtable;

public class Captions {
	public enum TitleCaptions {
		Login,
		Main,
	}

	public enum HeaderCaptions {
		Page,
	}

	public static Dictionary titles = new Hashtable<TitleCaptions, String>() {
		{
			put(TitleCaptions.Login, "Logowanie");
			put(TitleCaptions.Main, "MyPGS");
		}
	};

	public static Dictionary headers = new Hashtable<HeaderCaptions, String>() {
		{
			put(HeaderCaptions.Page, "MYPGS");
		}
	};
}
