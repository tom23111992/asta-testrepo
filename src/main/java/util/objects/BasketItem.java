package util.objects;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by ttutka on 7/19/2016.
 */
public class BasketItem {
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String price;
    @Getter
    @Setter
    private String quantity;

//    public String getName() {
//        return name;
//    }
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getPrice() {
//        return price;
//    }
//    public void setPrice(String price) {
//        this.price = price;
//    }
//
//    public String getQuantity() {
//        return quantity;
//    }
//    public void setQuantity(String quantity) {
//        this.quantity = quantity;
//    }


}
