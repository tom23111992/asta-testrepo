package util.objects;

import lombok.Getter;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ttutka on 10/4/2016.
 */
public class Person {

    @Getter
    @Setter
    private List<PersonDetails> details;

    public Person() {
        this.details = new LinkedList<>();
    }

}
