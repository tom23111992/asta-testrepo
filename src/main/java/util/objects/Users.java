package util.objects;

import util.configurations.TestConfiguration;

public class Users {
	private static String[] pageDefaultUser = TestConfiguration.pageDefaultUser;
	public final static Users DEFAULT = new Users(pageDefaultUser[0], pageDefaultUser[1]);

	public static Users RECENT;	// distinction from Java naming convention on purpose

	// Defs
	public String username;
	public String password;

	public Users(String common)
	{
		this.username = common;
		this.password = common;
	}

	public Users(String username, String password)
	{
		this.username = username;
		this.password = password;
	}
}
