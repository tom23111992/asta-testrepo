package util.objects;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by ttutka on 10/4/2016.
 */
public class PersonDetails {
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String surname;
    @Getter
    @Setter
    private String notes;
    @Getter
    @Setter
    private String phoneNumber;


}
