package util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.MarionetteDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.configurations.TestConfiguration;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class SeleniumExecutor {
    private final static int TIMEOUT = 120; //seconds
    public static WebDriver driver;
    public static String pageDefaultUrl;
    public static String pageZad1;
    private static SeleniumExecutor executor;
    private static WebDriverWait waitDriver;
    private static String parentWindowHandle;
    private static Iterator<String> windowIterator;

    public SeleniumExecutor() {
        TestConfiguration.setProperties();

        try {
            this.driver = createDriver();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        this.parentWindowHandle = driver.getWindowHandle();

        this.pageDefaultUrl = TestConfiguration.pageDefaultUrl;
        this.pageZad1 = TestConfiguration.pageZad1;
    }

    public static void stop() {
        driver.close();
        driver.quit();
        executor = null;
    }

    private static void start() {
        if (executor == null)
            executor = new SeleniumExecutor();
    }

    public static SeleniumExecutor getExecutor() {
        start();
        return executor;
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public static WebDriverWait getWaitDriver() {
        return waitDriver;
    }

    public static String getParentWindowHandle() {
        return parentWindowHandle;
    }

    public static Iterator<String> getWindowIterator() {
        return windowIterator;
    }

    public static void setWindowIterator() {
        windowIterator = driver.getWindowHandles().iterator();
    }

    private static WebDriver createDriver() throws MalformedURLException {

        String browser;


        if (System.getProperty("browser") == null) {
            browser = "chrome";
//            browser = PropertiesHelper.getProperty("environment");
        } else {
            browser = System.getProperty("browser");
        }

        String hub = "http://192.168.99.100:4444//wd/hub";
        DesiredCapabilities desiredCapabilities;

        String system = System.getProperty("os.name");
        System.out.println(system);

        switch (browser) {
            case "chrome":

                if (system.equals("Linux")) {
                    System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/files/chromedriver");

                } else

                    System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\files\\chromedriver.exe");

                driver = new ChromeDriver();
                break;

            case "firefox":
                System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\files\\geckodriver.exe");
                driver = new MarionetteDriver();
                break;

            case "remotefirefox":
                desiredCapabilities = DesiredCapabilities.firefox();
                desiredCapabilities.setBrowserName("firefox");
                driver = new RemoteWebDriver(new URL(hub), desiredCapabilities);
                break;

            case "remotechrome":
                desiredCapabilities = DesiredCapabilities.chrome();
                desiredCapabilities.setBrowserName("chrome");
                driver = new RemoteWebDriver(new URL(hub), desiredCapabilities);
                break;
        }

        driver.manage().window().maximize();
        waitDriver = new WebDriverWait(driver, TIMEOUT);
        driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);

        return driver;
    }

    public static String getTitle() {
        return driver.getTitle();
    }

    public static String getUrl() {
        return driver.getCurrentUrl();
    }

    public void deleteCookies() {
        driver.manage().deleteAllCookies();
    }

    public void openPage(String url) {
        driver.navigate().to(url);
    }
}