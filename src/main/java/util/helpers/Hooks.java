package util.helpers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.objects.Basket;
import util.objects.BasketItem;

import java.sql.SQLException;

/**
 * Created by ttutka on 18/01/2017.
 */
public class Hooks {

    private static final Logger LOG = LoggerFactory.getLogger(DataSource.class);
    private Basket basket;

    public Hooks(Basket basket) {

        this.basket = basket;
    }

    public static int insertData(String summaryQuantity, String summaryPrice, Basket basket) throws SQLException {

        java.sql.PreparedStatement preparedStatement = null;
        int result = 0;

        String query = "INSERT INTO task1 (summary_quantity, summary_price, items) VALUES(?, ?, ?)";
        String desciption = "";

        for (BasketItem x : basket.getItems()) {

            desciption = desciption + x.getName() + "-" + x.getQuantity() + ",";
        }

        try {

            preparedStatement = DataSource.getConnection().prepareStatement(query);
            preparedStatement.setString(1, summaryQuantity);
            preparedStatement.setString(2, summaryPrice);
            preparedStatement.setString(3, desciption);

            result = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            LOG.error("Problem while executing query", e);
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        DataSource.closeConnection();
        return result;
    }
}