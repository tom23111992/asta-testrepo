package util.helpers;

/**
 * Created by ttutka on 9/1/2016.
 */
public class Files {


    public static final String Data_Path_Correct = System.getProperty("user.dir") + "\\files\\dane.txt";
    public static final String Data_Path_More_Verses = System.getProperty("user.dir") + "\\files\\dane_more_than_20.txt";
    public static final String Data_Path_More_Number = System.getProperty("user.dir") + "\\files\\dane_number_has_more_than_9.txt";
    public static final String Data_Path_Jpg_Format = System.getProperty("user.dir") + "\\files\\1.jpg";
    public static final String Data_Path_Ppt_Format = System.getProperty("user.dir") + "\\files\\ppt.ppt";
    public static final String Data_Path_Pdf_Format = System.getProperty("user.dir") + "\\files\\pdf.pdf";

}
