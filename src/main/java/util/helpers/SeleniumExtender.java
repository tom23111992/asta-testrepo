package util.helpers;

import com.google.common.base.Function;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.SeleniumExecutor;
import util.enums.AttributeType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import static util.helpers.SeleniumExtender.SeleniumGetAttribute.getElementAttribute;
import static util.helpers.SeleniumExtender.SeleniumGetElement.getElementWithTextFromElementList;
import static util.helpers.SeleniumExtender.SeleniumScroll.scrollToElement;
import static util.helpers.SeleniumExtender.SeleniumSwitchWindow.*;
import static util.helpers.SeleniumExtender.SeleniumWait.*;

//import cucumber.api.java.After;
//import cucumber.api.java.Before;

public class SeleniumExtender {

    public static class SeleniumScroll {

        public static WebElement scrollToElement(WebElement element) {
            WebDriver driver = SeleniumExecutor.getDriver();
            String position = Integer.toString(element.getLocation().getY());
            String script = String.format("$('#main-container').scrollTop({0})", position);
            ((JavascriptExecutor) driver).executeScript(script);

            return element;
        }
    }

    public static class SeleniumClick {

        public static void clickWithWait(WebElement e) {
            waitForNoAjaxRequestsPending();
            scrollToElement(e);
            waitForElementToBeDisplayed(e);
            e.click();
            waitUntilSpinnerIsNotDisplayed();
        }

        public static void clickElementNumberFromElementList(List<WebElement> webElement, int item) {
            webElement.get(item).click();
        }

        public static void clickElementWithTextFromElementList(List<WebElement> links, String item) {
            try {
                links.stream().filter(element -> element.getText() == item).findFirst().get().click();
                //First(element => element.Text == item).Click();
            } catch (Exception e1) {
                try {
                    links.stream().filter(element -> element.getText().contains(item)).findFirst().get().click();
                    //.First(element => element.Text.Contains(item)).Click();
                } catch (Exception e2) {
                    links.stream().filter(element -> getElementAttribute(element, AttributeType.value).contains(item)).findFirst().get().click();
                    //.First(element => element.GetElementAttribute(AttributeType.value).Contains(item)).Click();
                }
            }
        }

        public static void clickElementRandomFromElementList(List<WebElement> webElements, int howMany) {
            for (int i = 0; i <= howMany; i++) {
                getElementWithTextFromElementList(webElements, "any").click();
            }
        }
    }

    public static class SeleniumSendKeys {

        public static void sendKeysWithWait(WebElement e, String text) {
            waitForNoAjaxRequestsPending();
            waitForElementToBeDisplayed(e);
            e.sendKeys(text);
        }
    }

    public static class SeleniumClear {

        public static void clearWithWait(WebElement e) {
            waitForNoAjaxRequestsPending();
            waitForElementToBeDisplayed(e);
            e.clear();
        }
    }

    public static class SeleniumGetText {

        public static List<String> getTextListFromElementList(List<WebElement> list) {
            List<String> finalList = new ArrayList<String>();

            for (WebElement element : list) {
                String text = element.getText();
                finalList.add(text);
            }
            return finalList;
        }

        public static String getTextFromElementList(List<WebElement> list) {
            return getTextFromElementList(list, "any");
        }

        public static String getTextFromElementList(List<WebElement> list, String caption) {
            if (caption.equals("any")) {
                Random r = new Random();
                caption = list.get(r.nextInt(list.size())).getText();
            }
            return caption;
        }
    }

    public static class SeleniumFind {

        public static WebElement findElementWithWait(WebElement e, By by) {
            waitForNoAjaxRequestsPending();
            WebElement element = e.findElement(by);
            scrollToElement(element);
            return element;
        }

        public static List<WebElement> findElementsWithWait(WebElement e, By by) {
            waitForNoAjaxRequestsPending();
            return e.findElements(by);
        }
    }

    public static class SeleniumGetElement {

        public static WebElement getElementRandomFromElementList(List<WebElement> list) {
            return getElementWithTextFromElementList(list, "any");
        }

        public static WebElement getElementWithTextFromElementList(List<WebElement> list, String caption) {
            if (caption.equals("any")) {
                Random r = new Random();
                caption = list.get(r.nextInt(list.size())).getText();
            }
            final String finalCaption = caption;
            return list.stream().filter(element -> element.getText().contains(finalCaption)).findFirst().get();
            //.First(x => x.Text.Contains(caption));
        }
    }

    public static class SeleniumGetAttribute {

        public static String getElementAttribute(WebElement webElement, AttributeType attribute) {
            return webElement.getAttribute(attribute.toString());
        }

        public static String getAttributeWithWait(WebElement e, String attribute) {
            waitForNoAjaxRequestsPending();
            return e.getAttribute(attribute);
        }
    }

    //NOTE: methods not tested under Java solution
    public static class SeleniumWait {

        public static void waitForNoAjaxRequestsPending() {
            WebDriverWait waitDriver = SeleniumExecutor.getWaitDriver();
            boolean isTrue = false;

            while (!isTrue) {
                try {
                    String script = String.format("return document.readyState");
                    isTrue = ((JavascriptExecutor) waitDriver).executeScript(script).toString().equalsIgnoreCase("complete");
                    script = String.format("return jQuery.active == 0");
                    ((JavascriptExecutor) waitDriver).executeScript(script);
                } catch (Exception e) {
                    return;
                }
            }
        }

        public static void waitForElementToBeDisplayed(WebElement e) {
            WebDriverWait waitDriver = SeleniumExecutor.getWaitDriver();
            waitDriver.until((Function<? super WebDriver, Boolean>) d -> e.isDisplayed());
            //waitDriver.Until(d => (bool)e.Displayed == true);
        }

        public static void waitForElementToBeEnabled(WebElement e) {
            WebDriverWait waitDriver = SeleniumExecutor.getWaitDriver();
            waitDriver.until((Function<? super WebDriver, Boolean>) d -> e.isEnabled());
            //waitDriver.Until(d => (bool)e.Enabled == true);
        }

        public static void waitForElementToBeNotDisplayed(By by) {
            WebDriverWait waitDriver = SeleniumExecutor.getWaitDriver();
            WebElement element = null;

            try {
                waitDriver.until((Function<? super WebDriver, Boolean>) d -> element.findElement(by).isDisplayed());
                //waitDriver.Until(d => d.FindElement(by).Displayed == false);
            } catch (Exception e) {
                waitDriver.until((Function<? super WebDriver, Boolean>) d -> element.findElements(by).size() == 0);
                //waitDriver.Until(d => d.FindElements(by).Count == 0);
            }
        }

        public static void waitUntilSpinnerIsNotDisplayed() {
            WebDriver driver = SeleniumExecutor.getDriver();
            List<WebElement> list = new ArrayList<WebElement>();

            do {
                list = driver.findElements(By.id("global-progress-indicator")); //ID of element which is a ProgressBar
            } while (list.size() != 0);
        }
    }

    public static class SeleniumIsDisplayed {

        public static boolean isElementDisplayed(By by) {
            WebDriver driver = SeleniumExecutor.getDriver();
            List<WebElement> list = driver.findElements(by);

            if (list.size() == 0) {
                return false;
            } else {
                return list.get(0).isDisplayed();
            }
        }
    }

    public static class SeleniumIsPresent {

        public static boolean isElementPresent(WebElement webElement) {
            try {
                boolean isPresent = webElement.isDisplayed();
                return isPresent;
            } catch (NoSuchElementException e) {
                return false;
            }
        }
    }

    public static class SeleniumAlertHandle {

        public static void alertHandle(boolean accept) {
            WebDriver driver = SeleniumExecutor.getDriver();
            //This method helps to handle confirm popup window
            if (accept == false) {
                driver.switchTo().alert().dismiss();
            } else if (accept == true) {
                driver.switchTo().alert().accept();
            }
        }
    }

    public static class SeleniumFrame {

        public static void sendKeysToFrameAny(String message) {
            WebDriver driver = SeleniumExecutor.getDriver();
            sendKeysToFrameNumber(message, 0);
        }

        public static void sendKeysToFrameNumber(String message, int frameNumber) {
            WebDriver driver = SeleniumExecutor.getDriver();
            driver.switchTo().frame(driver.findElements(By.xpath("//iframe")).get(frameNumber));

            WebElement textInput = driver.findElement(By.id("tinymce"));
            textInput.clear();
            textInput.click();
            textInput.sendKeys(message);

            driver.switchTo().window(driver.getWindowHandle());
        }

        public static List<String> getTextListFromFrame() {
            WebDriver driver = SeleniumExecutor.getDriver();
            List<String> text = new ArrayList<String>();

            List<WebElement> elements = driver.findElements(By.xpath("//iframe"));

            for (WebElement element : elements) {
                try {
                    if (text.isEmpty())// || text.stream () x => string.IsNullOrEmpty(x)))
                    {
                        driver.switchTo().frame(element);
                        List<WebElement> textInputs = driver.findElements(By.tagName("div"));

                        for (WebElement e : textInputs)
                            text.add(e.getText());

                        driver.switchTo().window(driver.getWindowHandle());
                    }
                } catch (Exception e) {
                    driver.switchTo().window(driver.getWindowHandle());
                }
            }

            return text;
        }
    }

    public static class SeleniumSwitchWindow {

        public static WebDriver switchToWindow(String windowHandle) {
            WebDriver driver = SeleniumExecutor.getDriver();
            WebDriver tmp = driver.switchTo().window(windowHandle);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return tmp;
        }

        public static void switchToParentWindow() {
            switchToWindow(SeleniumExecutor.getParentWindowHandle());
        }

        public static boolean switchToAnyNotParentWindow() {
            SeleniumExecutor.setWindowIterator();
            Iterator<String> windowIterator = SeleniumExecutor.getWindowIterator();
            String parentWindowHandle = SeleniumExecutor.getParentWindowHandle();

            while (windowIterator.hasNext()) {
                String windowHandle = windowIterator.next();

                if (!windowHandle.equals(parentWindowHandle)) {
                    switchToWindow(windowHandle);
                    return true;
                }
            }

            switchToParentWindow();
            return false;
        }

        public static boolean switchToWindowWithUrl(String url) {
            if (SeleniumExecutor.getUrl().contains(url))
                return true;

            WebDriver popup = null;
            SeleniumExecutor.setWindowIterator();
            Iterator<String> windowIterator = SeleniumExecutor.getWindowIterator();

            while (windowIterator.hasNext()) {
                String windowHandle = windowIterator.next();

                popup = switchToWindow(windowHandle);
                if (popup.getCurrentUrl().contains(url)) {
                    return true;
                }
            }

            switchToParentWindow();
            return false;
        }

        public static boolean switchToWindowWithTitle(String title) {
            if (SeleniumExecutor.getTitle().contains(title))
                return true;

            WebDriver popup = null;
            SeleniumExecutor.setWindowIterator();
            Iterator<String> windowIterator = SeleniumExecutor.getWindowIterator();

            while (windowIterator.hasNext()) {
                String windowHandle = windowIterator.next();

                popup = switchToWindow(windowHandle);
                if (popup.getTitle().contains(title)) {
                    return true;
                }
            }

            switchToParentWindow();
            return false;
        }
    }

    public static class SeleniumCloseWindow {

        public static void closeWindow() {
            WebDriver driver = SeleniumExecutor.getDriver();
            driver.close();
            driver.navigate().to("");
        }

        public static void closeAnyNotParentWindow() {
            while (switchToAnyNotParentWindow()) {
                SeleniumExecutor.getDriver().close();
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            switchToParentWindow();
        }
    }

    public static class SeleniumIsWindow {

        public static boolean isWindowWithTitlePresent(String title) {
            boolean isPresent = false;
            String recentWindow;
            String parentWindowHandle = SeleniumExecutor.getParentWindowHandle();

            try {
                recentWindow = SeleniumExecutor.getDriver().getWindowHandle();
            } catch (NoSuchWindowException e) {
                recentWindow = parentWindowHandle;
            }

            switchToWindow(recentWindow);

            if (SeleniumExecutor.getTitle().contains(title)) {
                isPresent = true;
            } else {
                WebDriver popup = null;
                SeleniumExecutor.setWindowIterator();
                Iterator<String> windowIterator = SeleniumExecutor.getWindowIterator();

                while (windowIterator.hasNext()) {
                    String windowHandle = windowIterator.next();

                    popup = switchToWindow(windowHandle);
                    if (popup.getTitle().contains(title)) {
                        isPresent = true;
                        break;
                    }
                }
            }

            switchToWindow(recentWindow);
            return isPresent;
        }

        public static boolean isWindowWithUrlPresent(String url) {
            boolean isPresent = false;
            String recentWindow;
            String parentWindowHandle = SeleniumExecutor.getParentWindowHandle();

            try {
                recentWindow = SeleniumExecutor.getDriver().getWindowHandle();
            } catch (NoSuchWindowException e) {
                recentWindow = parentWindowHandle;
            }

            switchToWindow(recentWindow);

            if (SeleniumExecutor.getUrl().contains(url)) {
                isPresent = true;
            } else {
                WebDriver popup = null;
                SeleniumExecutor.setWindowIterator();
                Iterator<String> windowIterator = SeleniumExecutor.getWindowIterator();

                while (windowIterator.hasNext()) {
                    String windowHandle = windowIterator.next();

                    popup = switchToWindow(windowHandle);
                    if (popup.getCurrentUrl().contains(url)) {
                        isPresent = true;
                        break;
                    }
                }
            }

            switchToWindow(recentWindow);
            return isPresent;
        }
    }

    public static class SeleniumReporter {

        private static SeleniumReporter reporter;
        private Scenario scenario;

        public SeleniumReporter() {
        }

        private SeleniumReporter(Scenario scenario) {
            this.scenario = scenario;
        }

        public static void log(String message) {
            reporter.scenario.write(message);
        }

        private static byte[] getScreenshot() {

            byte[] screenshot = null;

            try {
                screenshot = ((TakesScreenshot) SeleniumExecutor.getDriver()).getScreenshotAs(OutputType.BYTES);
            } catch (WebDriverException somePlatformsDontSupportScreenshots) {
                System.err.println(somePlatformsDontSupportScreenshots.getMessage());
            }

            return screenshot;
        }

        public static void embedScreenshot() {
            reporter.scenario.embed(getScreenshot(), "image/png");
        }

        public static void embedScreenshotIfScenarioFailed() {
            if (reporter.scenario.isFailed())
                embedScreenshot();
        }

        private void init(Scenario scenario) {
            if (reporter == null)
                reporter = new SeleniumReporter(scenario);
        }

        private void close() {
            reporter = null;
        }

        @Before(order = 0)
        public void initReporter(Scenario newScenario) {
            init(newScenario);
        }

        @After(order = 99999)
        public void closeReporter() {
            embedScreenshotIfScenarioFailed();
            close();
        }


    }
}

