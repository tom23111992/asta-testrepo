package util.helpers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by ttutka on 18/01/2017.
 */
public class DataSource {

    private static final Logger LOG = LoggerFactory.getLogger(DataSource.class);

    private static String connectionUrl;
    private static Connection connection;

    private static String dbDefaultUser;
    private static String dbDefaultPassword;

    public static Connection getConnection() {
        try {
            if (connection == null || connection.isClosed()) {

                dbDefaultUser = "root";
                dbDefaultPassword = "";

                connectionUrl = "jdbc:mysql://localhost/asta";
                System.out.println(connectionUrl);
                try {

                    try {
                        Class.forName("com.mysql.jdbc.Driver");
                    } catch (ClassNotFoundException e) {
                        LOG.error("No JDBC Driver detected", e);
                    }
                    //doSshTunnel();
                    connection = DriverManager.getConnection(connectionUrl, dbDefaultUser, dbDefaultPassword);
                    if (connection != null)
                        LOG.info("Database connection successful");

                } catch (SQLException e) {
                    LOG.error("Database connection not created", e);
                }
            }
        } catch (SQLException e) {
            LOG.error("Unable to verify Connection state", e);
        }
        return connection;
    }

    // close connection to DB
    public static void closeConnection() {
        if (connection != null) try {
            connection.close();
        } catch (SQLException e) {
            LOG.error("Problem while close connection", e);
        }
    }
}