package util.helpers;


import java.lang.reflect.Field;
import java.util.*;

public class Comparer {
	public boolean compare(Object obj1, Object obj2) {
		return compareTwoObject(obj1, obj2, new String[]{}).succeed;
	}

	public boolean compare(Object obj1, Object obj2, String[] exclude) {
		return compareTwoObject(obj1, obj2, exclude).succeed;
	}

	public Result compareWithInfo(Object obj1, Object obj2) {
		return compareTwoObject(obj1, obj2, new String[]{});
	}

	public Result compareWithInfo(Object obj1, Object obj2, String[] exclude) {
		return compareTwoObject(obj1, obj2, exclude);
	}

	private Result compareTwoObject(Object obj1, Object obj2, String[] exclude) {
		Result result = new Result();

		Class<?> type = obj1.getClass();
		if (Boolean.class.isAssignableFrom(type)) {
			if ((boolean) obj1 != (boolean) obj2) {
				result.addError("Boolean value mismatch", obj1.toString(), obj2.toString());
			}
			return result;
		} else if (Integer.class.isAssignableFrom(type)) {
			if (Integer.compare((int) obj1, (int) obj2) != 0) {
				result.addError("Integer value mismatch", obj1.toString(), obj2.toString());
			}
			return result;
		} else if (Double.class.isAssignableFrom(type)) {
			if (Double.compare((double) obj1, (double) obj2) != 0) {
				result.addError("Double value mismatch", obj1.toString(), obj2.toString());
			}
			return result;
		} else if (Float.class.isAssignableFrom(type)) {
			if (Float.compare((float) obj1, (float) obj2) != 0) {
				result.addError("Float value mismatch", obj1.toString(), obj2.toString());
			}
			return result;
		} else if (String.class.isAssignableFrom(type)) {
			if (((String) obj1).compareToIgnoreCase((String) obj2) != 0) {
				result.addError("String value mismatch", obj1.toString(), obj2.toString());
			}
			return result;
		}

		Field[] tmpProperties = obj1.getClass().getDeclaredFields();
		Field[] properties = (Field[]) removeLastElementFromTable(tmpProperties);

		try {
			for (Field property : properties) {
				if (property != null) {
					property.setAccessible(true);

					if (!Arrays.asList(exclude).contains(property.getName())) {
						if (isPropertyACollection(property) && !String.class.isAssignableFrom(property.getType())) {
							List<?> firstList = null;
							List<?> secondList = null;
							firstList = (List<?>) property.get(obj1);
							secondList = (List<?>) property.get(obj2);
							//var firstArray = properties[i].GetValue(obj1) as IEnumerable;
							//var firstList = firstArray.Cast < Object > ().ToList();

							//var secondArray = properties[i].GetValue(obj2) as IEnumerable;
							//var secondList = secondArray.Cast < Object > ().ToList();

							if (!firstList.isEmpty() && !secondList.isEmpty()) {
								if (firstList.get(0).getClass().isPrimitive() && !firstList.containsAll(secondList)) {
									result.addError(property.getName(), property.get(obj1).toString(), property.get(obj2).toString());
								} else {
									if (firstList.size() == secondList.size()) {
										if (!compareTwoList(firstList, secondList, exclude)) {
											result.addError(property.getName(), property.get(obj1).toString(), property.get(obj2).toString());
										}
									} else {
										result.addError(property.getName(), property.get(obj1).toString(), property.get(obj2).toString());
									}
								}
							}
						} else {
							if (!property.getClass().isPrimitive() && !String.class.isAssignableFrom(property.getType())) {
								if (compare(property.get(obj1), null) && compare(property.get(obj2), null)) {
									result.addErrors(compareWithInfo(property.get(obj1), property.get(obj2), exclude).errors);
								}
							} else if (!compare(property.get(obj1), property.get(obj2))) {
								result.addError(property.getName(), property.get(obj1).toString(), property.get(obj2).toString());
							}
						}
					}
				}
			}

		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return result;
	}

	public boolean compareTwoList(List<?> firstList, List<?> secondList, String[] exclude) {
		for (int i = 0; i < firstList.size(); i++) {
			int index = findIndex(firstList.get(i), secondList, exclude);
			if (index >= 0) {
				secondList.remove(secondList.get(index));
			} else {
				return false;
			}
		}

		return (secondList.size() == 0);
	}

	public int findIndex(Object p, List<?> objects, String[] exclude) {
		for (int i = 0; i < objects.size(); i++) {
			if (compareWithInfo(p, objects.get(i), exclude).succeed)
				return i;
		}
		return -1;
	}

	private static boolean isPropertyACollection(Field property) {
		return Collection.class.isAssignableFrom(property.getType());
		//return property.PropertyType.GetInterface(typeof(IEnumerable).FullName) != null;
	}

	private static Object[] removeLastElementFromTable(Object[] input) {
		List result = new LinkedList();

		for (int i = 0; i < input.length - 1; i++)
			result.add(input[i]);

		return result.toArray(input);
	}

	public class Result {
		public List<singleError> errors;
		public boolean succeed;

		//public bool succeed { get; set; }

		public Result() {
			errors = new ArrayList<singleError>();
			succeed = true;
		}

		public void addError(String fieldName, String firstValue, String secondValue) {
			errors.add(new singleError(fieldName, firstValue, secondValue));
			succeed = false;
		}

		public void addErrors(List<singleError> errorsList) {
			errors.addAll(errorsList);
			if (errorsList.size() > 0) {
				succeed = false;
			}
		}

		public String printErrors() {
			String msg = "";
			for (singleError se : errors) {
				msg = msg + "\n" + se.fieldName + ": " + se.firstValue + " | " + se.secondValue;
			}
			return msg;
		}
	}

	public class singleError {

		public String getFieldName() {
			return fieldName;
		}

		public void setFieldName(String fieldName) {
			this.fieldName = fieldName;
		}

		private String fieldName;

		public String getFirstValue() {
			return firstValue;
		}

		public void setFirstValue(String firstValue) {
			this.firstValue = firstValue;
		}

		private String firstValue;

		public String getSecondValue() {
			return secondValue;
		}

		public void setSecondValue(String secondValue) {
			this.secondValue = secondValue;
		}

		private String secondValue;

		public singleError(String fieldName, String firstValue, String secondValue) {
			this.fieldName = fieldName;
			this.firstValue = firstValue;
			this.secondValue = secondValue;
		}
	}
}
