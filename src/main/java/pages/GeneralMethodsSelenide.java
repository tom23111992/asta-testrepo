package pages;

import com.codeborne.selenide.Configuration;

import static com.codeborne.selenide.Selenide.open;

/**
 * Created by ttutka on 19/01/2017.
 */
public class GeneralMethodsSelenide {

    public void openPageSelenide(String page) {

        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\files\\chromedriver.exe");
        Configuration.browser = "chrome";
        open("https://testingcup.pgs-soft.com/task_" + page);
    }
}
