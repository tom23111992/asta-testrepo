package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by ttutka on 7/13/2016.
 */
public class TaskThreeLocators {

    @FindBy(how = How.XPATH, using = "//li[@class='dropdown']")
    public WebElement menuList;

    @FindBy(how = How.XPATH, using = "//*[@id='menu1']/li[1]")
    public WebElement formButton;

    @FindBy(how = How.ID, using = "start-edit")
    public WebElement editButton;

    @FindBy(how = How.ID, using = "in-name")
    public WebElement nameField;

    @FindBy(how = How.ID, using = "in-surname")
    public WebElement surnameField;

    @FindBy(how = How.ID, using = "in-notes")
    public WebElement noteField;

    @FindBy(how = How.ID, using = "in-phone")
    public WebElement phoneNumberField;

    @FindBy(how = How.ID, using = "in-file")
    public WebElement chooseFileButton;

    @FindBy(how = How.ID, using = "save-btn")
    public WebElement saveButton;

    @FindBy(how = How.XPATH, using = "//div[@class='notifyjs-bootstrap-base notifyjs-bootstrap-success']//span")
    public WebElement informationAfterSavedForm;

    @FindBy(how = How.XPATH, using = "//img[@class='preview-photo']")
    public WebElement photo;
}
