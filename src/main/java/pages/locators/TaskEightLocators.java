package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by ttutka on 7/20/2016.
 */
public class TaskEightLocators {


    @FindBy(how = How.ID, using = "task8_form_cardType")
    public WebElement typeCardDropDownList;

    @FindBy(how = How.ID, using = "task8_form_name")
    public WebElement nameField;

    @FindBy(how = How.ID, using = "task8_form_cardNumber")
    public WebElement numberCardField;

    @FindBy(how = How.ID, using = "task8_form_cardCvv")
    public WebElement codeCvvField;

    @FindBy(how = How.ID, using = "task8_form_cardInfo_month")
    public WebElement monthCardDropDownList;

    @FindBy(how = How.ID, using = "task8_form_cardInfo_year")
    public WebElement yearCardDropDownList;

    @FindBy(how = How.XPATH, using = "//button[@name='task8_form[save]']")
    public WebElement payButton;

    @FindBy(how = How.XPATH, using = "//ul[@class='list-unstyled']/li")
    public WebElement payInformation;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'termin ważności karty')]")
    public WebElement termOfValidityCard;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'format CVV')]")
    public WebElement cvvFormat;

}
