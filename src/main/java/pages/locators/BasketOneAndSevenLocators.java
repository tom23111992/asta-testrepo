package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

/**
 * Created by ttutka on 15/12/2016.
 */
public class BasketOneAndSevenLocators {

    @FindBy(how = How.XPATH, using = "//button[text()='Dodaj']")
    public List<WebElement> buttonAddList;

    @FindBy(how = How.XPATH, using = "//div[@class='input-group input-group-sm']/input")
    public List<WebElement> enterFieldList;

    @FindBy(how = How.XPATH, using = "//div[@class='col-md-12 basket-summary']/p/span[@class='summary-quantity']")
    public WebElement summaryQuantity;

    @FindBy(how = How.XPATH, using = "//div[@class='col-md-12 basket-summary']/p/span[@class='summary-price']")
    public WebElement summaryPrice;

    @FindBy(how = How.XPATH, using = "//div[@class='caption']/h4")
    public List<WebElement> nameProductsList;

    @FindBy(how = How.XPATH, using = "//div[@class='caption']/p[1]")
    public List<WebElement> priceProductsList;

    @FindBy(how = How.XPATH, using = "//span[@class='input-group-btn']/button[text()='Usuń']")
    public List<WebElement> deleteButtonList;

    @FindBy(how = How.XPATH, using = "//div[@class='row row-in-basket']/div[@class='col-md-9 text-on-button-level']/span")
    public List<WebElement> quantityInBasketList;

    @FindBy(how = How.XPATH, using = "//div[@class='row row-in-basket']/div[@class='col-md-9 text-on-button-level']")
    public List<WebElement> nameAndPriceProductsInBasketList;

    @FindBy(how = How.XPATH, using = "//div[@class='draggable ui-draggable ui-draggable-handle']")
    public List<WebElement> dragElementPhoto;

    @FindBy(how = How.XPATH, using = "//div[@class='col-md-12 place-to-drop ui-droppable']")
    public WebElement dropElementBasket;

}
