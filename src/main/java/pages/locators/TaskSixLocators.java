package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by ttutka on 7/19/2016.
 */
public class TaskSixLocators {


    @FindBy(how = How.ID, using = "LoginForm__username")
    public WebElement logInField;

    @FindBy(how = How.ID, using = "LoginForm__password")
    public WebElement passwordField;

    @FindBy(how = How.ID, using = "LoginForm_save")
    public WebElement logInButton;

    @FindBy(how = How.XPATH, using = "//div[@class='task']//a[@href='/task_6/download']")
    public WebElement downloadButton;

    @FindBy(how = How.ID, using = "logout")
    public WebElement logOutButton;

    @FindBy(how = How.XPATH, using = "//ul[@class='list-unstyled']/li")
    public WebElement informationAboutFailLogIn;

}
