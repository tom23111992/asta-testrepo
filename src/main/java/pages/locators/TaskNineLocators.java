package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by ttutka on 7/21/2016.
 */
public class TaskNineLocators {

    @FindBy(how = How.XPATH, using = "//h1")
    public WebElement title;

    @FindBy(how = How.XPATH, using = "//*[@id='j1_1']/i")
    public WebElement expandParent;

    @FindBy(how = How.XPATH, using = "//*[@id='j1_1']/a")
    public WebElement rightClickToParent;

    @FindBy(how = How.XPATH, using = "//ul[@class='vakata-context jstree-contextmenu jstree-default-contextmenu']")
    public WebElement clickRename;

    @FindBy(how = How.CSS, using = "input.jstree-rename-input")
    public WebElement textRenameField;

    @FindBy(how = How.XPATH, using = "//*[@id='j1_2']/i")
    public WebElement expandChildFirst;

    @FindBy(how = How.XPATH, using = "//*[@id='j1_2']/a")
    public WebElement rightClickChildFirst;

    @FindBy(how = How.XPATH, using = "//*[@id='j1_3']/a")
    public WebElement rightClickChildWhereFirstIsParent;

    @FindBy(how = How.XPATH, using = "//*[@id='j1_4']/a")
    public WebElement rightClickLastChild;

}
