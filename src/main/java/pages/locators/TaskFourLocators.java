package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by ttutka on 7/13/2016.
 */
public class TaskFourLocators {

    @FindBy(how = How.XPATH, using = "//div[@class='col-md-3']/button")
    public WebElement applicationButton;

    @FindBy(how = How.XPATH, using = "//*[@name='name']")
    public WebElement nameField;

    @FindBy(how = How.XPATH, using = "//*[@name='email']")
    public WebElement emailField;

    @FindBy(how = How.XPATH, using = "//*[@name='phone']")
    public WebElement phoneNumberField;

    @FindBy(how = How.ID, using = "save-btn")
    public WebElement saveButton;

    @FindBy(how = How.CSS, using = "iframe")
    public WebElement frame;

    @FindBy(how = How.XPATH,using ="//*[contains(text(),'Nieprawidłowy email')]" )
    public WebElement invalidMail;

    @FindBy(how = How.XPATH,using ="//*[contains(text(),'Zły format telefonu - prawidłowy: 600-100-200')]" )
    public WebElement invalidPhoneNumber;

    @FindBy(how = How.XPATH, using = "//h1")
    public WebElement finishCommand;

}
