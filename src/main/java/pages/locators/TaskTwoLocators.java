package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

/**
 * Created by ttutka on 7/13/2016.
 */
public class TaskTwoLocators {

    @FindBy(how = How.XPATH, using = "//span[@class='select2-selection__rendered']")
    public WebElement dropDownList;

    @FindBy(how = How.XPATH, using = "//span[@class='select2-results']/ul/li")
    public List<WebElement> fieldsSortByList;

    @FindBy(how = How.XPATH, using = "//div[@class='caption']/p[3]")
    public List<WebElement> productsNameList;

    @FindBy(how = How.XPATH, using = "//span[@class='select2-search select2-search--dropdown']/input")
    public WebElement searchField;

    @FindBy(how = How.ID, using = "bs-example-navbar-collapse-9']/div[2]/a")
    public WebElement click;

}
