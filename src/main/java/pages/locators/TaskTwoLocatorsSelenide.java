package pages.locators;


import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;


/**
 * Created by ttutka on 19/01/2017.
 */
public class TaskTwoLocatorsSelenide {

    public SelenideElement dropDownList =$(By.xpath("//span[@class='select2-selection__rendered']"));
    public SelenideElement searchField=$(".select2-search__field");
    public ElementsCollection fieldsSortByList=$$(By.xpath("//span[@class='select2-results']/ul/li"));
    public ElementsCollection itemsNameList=$$(By.xpath("//div[@class='caption']/p[3]"));
}
