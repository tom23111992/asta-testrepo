package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

/**
 * Created by ttutka on 7/18/2016.
 */
public class TaskFiveLocators {

    @FindBy(how = How.XPATH, using = "//input")
    public WebElement sendFileButton;

    @FindBy(how = How.XPATH, using = "//tr//td[1]")
    public List<WebElement> nameList;

    @FindBy(how = How.XPATH, using = "//tr//td[2]")
    public List<WebElement> surnameList;

    @FindBy(how = How.XPATH, using = "//tr//td[3]")
    public List<WebElement> phoneList;
}
