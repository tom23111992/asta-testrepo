package pages;

import org.openqa.selenium.JavascriptExecutor;
import util.SeleniumExecutor;

public class AbstractPage {
    public static final String EMPTY_STRING = "";

    public SeleniumExecutor executor;
    private String url;

    public AbstractPage(SeleniumExecutor executor, String url) {
        this.executor = executor;
        this.url = url;
    }

    public AbstractPage(SeleniumExecutor executor) {
        this.executor = executor;
        this.url = SeleniumExecutor.pageDefaultUrl;
    }

    public AbstractPage() {
    }

    public void openPage(String page) {
        executor.deleteCookies();
        executor.openPage("https://testingcup.pgs-soft.com/task_" + page);
    }

    public void scrollToElement(Object locator) {

        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) executor.getDriver();
        javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);", locator);
    }

}
