package pages.executors;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.AbstractPage;
import pages.locators.TaskEightLocators;
import util.SeleniumExecutor;

/**
 * Created by ttutka on 7/20/2016.
 */
public class TaskEightPage extends AbstractPage {
    public TaskEightLocators locators;

    WebDriverWait wait = new WebDriverWait(SeleniumExecutor.driver, 10);
    public TaskEightPage(SeleniumExecutor executor) {

        super(executor);
        locators = new TaskEightLocators();
        PageFactory.initElements(SeleniumExecutor.getDriver(), locators);
    }

    public void chooseTypeCard(String text) {
        locators.typeCardDropDownList.sendKeys(text);
    }

    public void enterName(String text) {
        locators.nameField.sendKeys(text);
    }

    public void enterCardNumber(String text) {
        locators.numberCardField.sendKeys(text);
    }

    public void enterCvvCode(String text) {
        locators.codeCvvField.sendKeys(text);
    }

    public void chooseMonth(String text) {
        locators.monthCardDropDownList.sendKeys(text);
    }

    public void chooseYear(String text) {
        locators.yearCardDropDownList.sendKeys(text);
    }

    public void clickPay() {
        locators.payButton.click();
    }

    public String getStatementAboutValidityCard() {

        wait.until(ExpectedConditions.visibilityOf(locators.termOfValidityCard));
        return locators.termOfValidityCard.getText();
    }

    public String getStatementAboutInvalidCVVCodeFormat() {

        wait.until(ExpectedConditions.visibilityOf(locators.cvvFormat));
        return locators.cvvFormat.getText();
    }

    public String getPerformOrderPaid() {

        wait.until(ExpectedConditions.visibilityOf(locators.payInformation));
        return locators.payInformation.getText();
    }
}
