package pages.executors;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import pages.AbstractPage;
import pages.locators.TaskNineLocators;
import util.SeleniumExecutor;

/**
 * Created by ttutka on 7/21/2016.
 */
public class TaskNinePage extends AbstractPage {

    public TaskNineLocators locators;
    public String title;

    Actions actions = new Actions(executor.getDriver());

    public TaskNinePage(SeleniumExecutor executor) {

        super(executor);
        locators = new TaskNineLocators();
        PageFactory.initElements(SeleniumExecutor.getDriver(), locators);
    }

    public void expandParentAndRename(String text) {

        title = text;
        locators.expandParent.click();
        actions.contextClick(locators.rightClickToParent).perform();
        locators.clickRename.click();
        locators.textRenameField.clear();
        locators.textRenameField.sendKeys(text);
        locators.textRenameField.sendKeys(Keys.RETURN);
    }

    public void expandChildAndRename(String text) {

        title = text;
        locators.expandChildFirst.click();
        actions.contextClick(locators.rightClickChildFirst).perform();
        locators.clickRename.click();
        locators.textRenameField.clear();
        locators.textRenameField.sendKeys(text);
        locators.textRenameField.sendKeys(Keys.RETURN);
    }

    public void renameChildFirstChild(String text) {

        title = text;
        actions.contextClick(locators.rightClickChildWhereFirstIsParent).perform();
        locators.clickRename.click();
        locators.textRenameField.clear();
        locators.textRenameField.sendKeys(text);
        locators.textRenameField.sendKeys(Keys.RETURN);
    }

    public void renameLastChild(String text) {

        title = text;
        actions.contextClick(locators.rightClickLastChild).perform();
        locators.clickRename.click();
        locators.textRenameField.clear();
        locators.textRenameField.sendKeys(text);
        locators.textRenameField.sendKeys(Keys.RETURN);
    }
}
