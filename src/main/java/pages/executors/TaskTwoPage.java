package pages.executors;

import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;
import pages.AbstractPage;
import pages.locators.TaskTwoLocators;
import util.SeleniumExecutor;
import util.helpers.RandomStringGenerator;

/**
 * Created by ttutka on 7/13/2016.
 */
public class TaskTwoPage extends AbstractPage {

    public TaskTwoLocators locators;
    String sortText;

    public TaskTwoPage(SeleniumExecutor executor) {
        super(executor);
        locators = new TaskTwoLocators();
        PageFactory.initElements(SeleniumExecutor.getDriver(), locators);
    }

    public void choosingSortBy(String category) {

        locators.dropDownList.click();

        for (int i = 0; i < locators.fieldsSortByList.size(); i++) {

            if (locators.fieldsSortByList.get(i).getText().equals(category)) {

                Assert.assertTrue(true);
                sortText = locators.fieldsSortByList.get(i).getText();
                locators.fieldsSortByList.get(i).click();
                break;
            }
        }
    }

    public void checkingSort() {

        for (int i = 0; i < locators.productsNameList.size(); i++) {

            Assert.assertEquals(sortText, locators.productsNameList.get(i).getText());
        }
    }

    public void fillSearchFieldCategory(String partWord) {

        locators.dropDownList.click();
        locators.searchField.sendKeys(partWord);
        sortText = locators.fieldsSortByList.get(0).getText();
        locators.fieldsSortByList.get(0).click();
    }

    public void typingNoExistsCategory() {

        locators.dropDownList.click();
        locators.searchField.sendKeys(RandomStringGenerator.getRandomWord());
    }

    public String getNoResult() {
        return locators.fieldsSortByList.get(0).getText();
    }
}

