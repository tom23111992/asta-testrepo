package pages.executors;

import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;
import pages.AbstractPage;
import pages.locators.TaskSixLocators;
import util.SeleniumExecutor;
import util.helpers.RandomStringGenerator;
import util.helpers.SeleniumExtender;

/**
 * Created by ttutka on 7/19/2016.
 */
public class TaskSixPage extends AbstractPage {

    public TaskSixLocators locators;

    public TaskSixPage(SeleniumExecutor executor) {

        super(executor);
        locators = new TaskSixLocators();
        PageFactory.initElements(SeleniumExecutor.getDriver(), locators);
    }

    public void fillLogIn(String text) {
        SeleniumExtender.SeleniumSendKeys.sendKeysWithWait(locators.logInField, text);
    }

    public void fillLogInRandom() {
        SeleniumExtender.SeleniumSendKeys.sendKeysWithWait(locators.logInField, RandomStringGenerator.getRandomWord());
    }

    public void fillPassword(String text) {
        SeleniumExtender.SeleniumSendKeys.sendKeysWithWait(locators.passwordField, text);
    }

    public void fillPasswordRandom() {
        SeleniumExtender.SeleniumSendKeys.sendKeysWithWait(locators.passwordField, RandomStringGenerator.getRandomWord());
    }

    public void logInButton() {
        locators.logInButton.click();
    }

    public void downloadButton() {
        locators.downloadButton.click();
    }

    public void logOutButton() {
        locators.logOutButton.click();
    }

    public boolean checkIfUserIsLogOut() {

        Assert.assertTrue(locators.logInField.isDisplayed());
        Assert.assertTrue(locators.passwordField.isDisplayed());
        Assert.assertTrue(locators.logInButton.isDisplayed());
        return true;
    }

    public String getInvalidLoginText() {

        Assert.assertTrue(locators.informationAboutFailLogIn.isDisplayed());
        return locators.informationAboutFailLogIn.getText();
    }
}
