package pages.executors;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.AbstractPage;
import pages.locators.TaskFourLocators;
import util.SeleniumExecutor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ttutka on 7/13/2016.
 */
public class TaskFourPage extends AbstractPage {

    public TaskFourLocators locators;
    public List<String> list = new ArrayList<String>();
    WebDriverWait wait = new WebDriverWait(SeleniumExecutor.driver, 5);

    public TaskFourPage(SeleniumExecutor executor) {

        super(executor);
        locators = new TaskFourLocators();
        PageFactory.initElements(SeleniumExecutor.getDriver(), locators);
    }

    public void openForm() {

        locators.applicationButton.click();
        list.addAll(SeleniumExecutor.getDriver().getWindowHandles());
    }

    public void switchToFirstWindow() {
        SeleniumExecutor.getDriver().switchTo().window(list.get(0));
    }

    public void fillNameAndSurname(String text) {

        SeleniumExecutor.getDriver().switchTo().window(list.get(1));
        SeleniumExecutor.getDriver().switchTo().frame(locators.frame);
        locators.nameField.sendKeys(text);
    }

    public void fillEmail(String string) {
        locators.emailField.sendKeys(string);
    }

    public void fillNumber(String string) {
        locators.phoneNumberField.sendKeys(string);
    }

    public void clickSaveData() {
        locators.saveButton.click();
    }

    public String getFinishMessage() {

        wait.until(ExpectedConditions.visibilityOf(locators.finishCommand));
        return locators.finishCommand.getText();
    }

    public String getInvalidMail() {
        return locators.invalidMail.getText();
    }

    public String getInvalidPhoneNumber() {
        return locators.invalidPhoneNumber.getText();
    }
}




