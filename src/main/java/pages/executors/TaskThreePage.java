package pages.executors;

import org.junit.Assert;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.AbstractPage;
import pages.locators.TaskThreeLocators;
import util.SeleniumExecutor;
import util.objects.Person;
import util.objects.PersonDetails;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ttutka on 7/13/2016.
 */
public class TaskThreePage extends AbstractPage {

    public TaskThreeLocators locators;
    Actions action = new Actions(executor.getDriver());
    WebDriverWait wait = new WebDriverWait(SeleniumExecutor.driver, 5);
    private Person person;

    public TaskThreePage(SeleniumExecutor executor) {

        super(executor);
        locators = new TaskThreeLocators();
        PageFactory.initElements(SeleniumExecutor.getDriver(), locators);
    }

    public void selectMenu() {

        action.moveToElement(locators.menuList);
        action.moveToElement(locators.formButton);
        action.click(locators.editButton);
        action.click(locators.nameField).perform();
    }

    public void enterName(String text) {

        locators.nameField.clear();
        locators.nameField.sendKeys(text);
    }

    public void enterSurname(String text) {

        locators.surnameField.clear();
        locators.surnameField.sendKeys(text);
    }

    public void enterNote(String text) {

        locators.noteField.clear();
        locators.noteField.sendKeys(text);
    }

    public void enterPhoneNumber(String text) {

        locators.phoneNumberField.clear();
        locators.phoneNumberField.sendKeys(text);
    }

    public void addFile(String text) {
        locators.chooseFileButton.sendKeys(text);
    }

    public void saveForm() {
        locators.saveButton.click();
    }

    public boolean checkDisplayInformationAboutSaveData() {

        wait.until(ExpectedConditions.elementToBeClickable(locators.informationAfterSavedForm));
        return true;
    }

    public String getInformationAfterSavedForm(){
        return locators.informationAfterSavedForm.getText();
    }

    public List<PersonDetails> getAddedPersonalDetails() {
        List<PersonDetails> details = new LinkedList<>();

        PersonDetails receivedPersonDetails = new PersonDetails();
        receivedPersonDetails.setName(locators.nameField.getAttribute("value"));
        receivedPersonDetails.setSurname(locators.surnameField.getAttribute("value"));
        receivedPersonDetails.setNotes(locators.noteField.getAttribute("value"));
        receivedPersonDetails.setPhoneNumber(locators.phoneNumberField.getAttribute("value"));
        details.add(receivedPersonDetails);

        return details;
    }

    public boolean checkDisabledField() {

        Assert.assertTrue(!ExpectedConditions.elementToBeClickable(locators.nameField).equals(true));
        Assert.assertTrue(!ExpectedConditions.elementToBeClickable(locators.surnameField).equals(true));
        Assert.assertTrue(!ExpectedConditions.elementToBeClickable(locators.phoneNumberField).equals(true));
        Assert.assertTrue(!ExpectedConditions.elementToBeClickable(locators.noteField).equals(true));

        return false;
    }

    public void checkAlert(String invalidFileMessage) {

        String alertText = executor.driver.switchTo().alert().getText();
        Assert.assertEquals(invalidFileMessage, alertText);
        executor.driver.switchTo().alert().accept();
    }
}

