package pages.executors;

import org.junit.Assert;
import pages.GeneralMethodsSelenide;
import pages.locators.TaskTwoLocatorsSelenide;
import util.helpers.RandomStringGenerator;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

/**
 * Created by ttutka on 19/01/2017.
 */
public class TaskTwoPageSelenide extends GeneralMethodsSelenide {

    String sortText;
    private TaskTwoLocatorsSelenide taskTwoLocatorsSelenide;

    public TaskTwoPageSelenide(TaskTwoLocatorsSelenide taskTwoLocatorsSelenide) {

        this.taskTwoLocatorsSelenide = taskTwoLocatorsSelenide;
    }

    private void dropdownListClick() {
        $(taskTwoLocatorsSelenide.dropDownList).click();
    }

    public void sortBy(String category) {

        dropdownListClick();
        for (int i = 0; i < $$(taskTwoLocatorsSelenide.fieldsSortByList).size(); i++) {

            if ($$(taskTwoLocatorsSelenide.fieldsSortByList).get(i).getText().equals(category)) {
                sortText = $$(taskTwoLocatorsSelenide.fieldsSortByList).get(i).getText();
                $$(taskTwoLocatorsSelenide.fieldsSortByList).get(i).click();
                break;
            }
        }
    }

    public void checkResults() {

        for (int i = 0; i < $$(taskTwoLocatorsSelenide.itemsNameList).size(); i++) {
            Assert.assertEquals("Soring is incorrect", sortText, $$(taskTwoLocatorsSelenide.itemsNameList).get(i).getText());
        }
    }

    public void typeExistsCategory(String text) {

        dropdownListClick();
        $(taskTwoLocatorsSelenide.searchField).setValue(text);
        sortText = $$(taskTwoLocatorsSelenide.fieldsSortByList).get(0).getText();
        $$(taskTwoLocatorsSelenide.fieldsSortByList).get(0).click();
    }

    public void typeNoExistsCategory() {

        dropdownListClick();
        $(taskTwoLocatorsSelenide.searchField).setValue(RandomStringGenerator.getRandomWord());
    }

    public String getNoResult() {
        return $$(taskTwoLocatorsSelenide.fieldsSortByList).get(0).getText();
    }
}
