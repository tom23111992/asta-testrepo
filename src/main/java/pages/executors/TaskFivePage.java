package pages.executors;

import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.AbstractPage;
import pages.locators.TaskFiveLocators;
import util.SeleniumExecutor;
import util.helpers.Files;

import java.io.*;

/**
 * Created by ttutka on 7/18/2016.
 */
public class TaskFivePage extends AbstractPage {

    public TaskFiveLocators locators;
    WebDriverWait wait = new WebDriverWait(SeleniumExecutor.driver, 5);
    private Files files;

    public TaskFivePage(SeleniumExecutor executor) {

        super(executor);
        locators = new TaskFiveLocators();
        PageFactory.initElements(SeleniumExecutor.getDriver(), locators);
    }

    public void addFile(String text) {
        locators.sendFileButton.sendKeys(text);
    }

    public void checkingAlert(String text) {

        wait.until(ExpectedConditions.alertIsPresent());
        String alert = executor.driver.switchTo().alert().getText();
        Assert.assertEquals(text, alert);
        executor.driver.switchTo().alert().accept();
    }

    public void assertionData() throws IOException {

        FileInputStream fileInputStream = new FileInputStream(files.Data_Path_Correct);
        DataInputStream dataInputStream = new DataInputStream(fileInputStream);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(dataInputStream));
        String st;
        int i = 0;
        while ((st = bufferedReader.readLine()) != null) {

            String splitted[] = st.split(",");
            Assert.assertEquals(locators.nameList.get(i).getText(), splitted[0]);
            Assert.assertEquals(locators.surnameList.get(i).getText(), splitted[1]);
            Assert.assertEquals(locators.phoneList.get(i).getText(), splitted[2]);
            i++;
        }
    }
}
