package pages.executors;

import org.junit.Assert;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import pages.AbstractPage;
import pages.locators.BasketOneAndSevenLocators;
import util.SeleniumExecutor;
import util.enums.DescriptionsMessages;
import util.helpers.SeleniumExtender;
import util.objects.Basket;
import util.objects.BasketItem;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ttutka on 15/12/2016.
 */
public class BasketOneAndSevenPage extends AbstractPage {

    int parseQunatityFromBasket = 0, parseQunatityFromScenario = 0;

    private Basket basket;
    private BasketItem basketItem;
    private BasketOneAndSevenLocators locators;
    private Actions actions = new Actions(executor.getDriver());
    private String finalStringQuantity;


    public BasketOneAndSevenPage(SeleniumExecutor executor) {
        super(executor);
        locators = new BasketOneAndSevenLocators();
        PageFactory.initElements(SeleniumExecutor.getDriver(), locators);
    }

    public void typeQuantity(int positionItem, String quantityProducts) {

        scrollToElement(locators.buttonAddList.get(positionItem));
        SeleniumExtender.SeleniumClear.clearWithWait(locators.enterFieldList.get(positionItem));
        SeleniumExtender.SeleniumSendKeys.sendKeysWithWait(locators.enterFieldList.get(positionItem), quantityProducts);
    }

    public String getProductPrice(int positionItem) {

        return locators.priceProductsList.get(positionItem).getText().split(" ")[1];
    }

    public String getProductName(int positionItem) {

        return locators.nameProductsList.get(positionItem).getText();
    }

    public String getTotalQuantityFromBasket() {

        return locators.summaryQuantity.getText();
    }

    public String getTotalPriceFromBasket() {

        String[] price = locators.summaryPrice.getText().split(" ");
        BigDecimal bigDecimal = new BigDecimal(price[0]);
        String summary = String.valueOf(bigDecimal.setScale(1, BigDecimal.ROUND_HALF_UP));

        return summary;
    }

    public void addToBasketByDragAndDrop(int numberAddButtonProduct) {

        actions.dragAndDrop(locators.dragElementPhoto.get(numberAddButtonProduct), locators.dropElementBasket).perform();
    }

    public void addToBasketByButton(int numberAddButtonProduct) {

        locators.buttonAddList.get(numberAddButtonProduct).click();
    }

    public void removeItemFromBasket() {

        locators.deleteButtonList.get(0).click();
    }

    public String calculatTotalPrice() {

        String partPrice;
        double summaryPrice = 0, partsummary = 0;
        Basket price = new Basket();

        for (int i = 0; i < locators.nameAndPriceProductsInBasketList.size(); i++) {

            String priceList[] = locators.nameAndPriceProductsInBasketList.get(i).getText().split("\n");
            String price1[] = priceList[0].split(" ");
            String price2[] = price1[1].split("\\(");
            String price3[] = price2[1].split("\\)");
            partPrice = price3[0];

            double quantityDouble = Double.parseDouble(locators.quantityInBasketList.get(i).getText());
            double partPriceDouble = Double.parseDouble(partPrice);
            partsummary = quantityDouble * partPriceDouble;
            summaryPrice = summaryPrice + partsummary;
        }

        BigDecimal bigDecimal = new BigDecimal(summaryPrice);
        String summary = String.valueOf(bigDecimal.setScale(1, BigDecimal.ROUND_HALF_UP));
        price.setTotalPrice(summary);

        return price.getTotalPrice();
    }

    public void checkingAlert() {

        String alert = executor.driver.switchTo().alert().getText();
        Assert.assertEquals(DescriptionsMessages.tooMuchQuantityProducts.toString(), alert);
        executor.driver.switchTo().alert().accept();
    }

    public List<BasketItem> getAddedBasketItems() {
        List<BasketItem> basketItems = new LinkedList<>();

        for (int i = 0; i < locators.nameAndPriceProductsInBasketList.size(); i++) {

            String nameProductInBasket[] = locators.nameAndPriceProductsInBasketList.get(i).getText().split(" ");
            String priceProductInBasket[] = locators.nameAndPriceProductsInBasketList.get(i).getText().split("\n");
            String price1[] = priceProductInBasket[0].split(" ");
            String price2[] = price1[1].split("\\(");
            String price3[] = price2[1].split("\\)");
            BasketItem basketItem = new BasketItem();

            basketItem.setName(nameProductInBasket[0]);
            basketItem.setPrice(price3[0]);
            basketItem.setQuantity(locators.quantityInBasketList.get(i).getText());
            basketItems.add(basketItem);

        }
        return basketItems;
    }

    public String getQuantity(String quantityFromObject, String quantityFromScenario) {

        parseQunatityFromBasket = Integer.parseInt(quantityFromObject);
        parseQunatityFromScenario = Integer.parseInt(quantityFromScenario);
        finalStringQuantity = Integer.toString(parseQunatityFromBasket + parseQunatityFromScenario);
        return finalStringQuantity;
    }
}
