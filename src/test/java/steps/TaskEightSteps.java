package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pages.executors.TaskEightPage;
import util.SeleniumExecutor;
import util.enums.DescriptionsMessages;

/**
 * Created by ttutka on 7/20/2016.
 */
public class TaskEightSteps {

    private TaskEightPage taskEightPage;

    public TaskEightSteps() {

        SeleniumExecutor executor = SeleniumExecutor.getExecutor();
        this.taskEightPage = new TaskEightPage(executor);
    }

    @When("^I select card type \"([^\"]*)\"$")
    public void choose_card_type(String text) throws Throwable {

        taskEightPage.chooseTypeCard(text);
    }

    @And("^I write name \"([^\"]*)\"$")
    public void enter_name(String text) throws Throwable {

        taskEightPage.enterName(text);
    }

    @And("^I write card number \"([^\"]*)\"$")
    public void enter_card_number(String text) throws Throwable {

        taskEightPage.enterCardNumber(text);
    }

    @And("^I write CVV code \"([^\"]*)\"$")
    public void enter_cvv_code(String text) throws Throwable {

        taskEightPage.enterCvvCode(text);
    }

    @And("^I select month \"([^\"]*)\"$")
    public void choose_month(String text) throws Throwable {

        taskEightPage.chooseMonth(text);
    }

    @And("^I select year \"([^\"]*)\"$")
    public void choose_year(String text) throws Throwable {

        taskEightPage.chooseYear(text);
    }

    @And("^I press pay button$")
    public void click_pay_button() throws Throwable {

        taskEightPage.clickPay();
    }

    @Then("^Should be displayed message about correct payment$")
    public void check_display_text() throws Throwable {

        Assert.assertEquals(DescriptionsMessages.orderPaid.toString(), taskEightPage.getPerformOrderPaid());
    }

    @Then("^Should be displayed information about pay cards pass the expiry date$")
    public void display_term_validity_card() {

        Assert.assertEquals(DescriptionsMessages.validityCard.toString(), taskEightPage.getStatementAboutValidityCard());
    }

    @Then("^Should be displayed information about incorrect format CVV code$")
    public void display_information_CVV_code() {

        Assert.assertEquals(DescriptionsMessages.incorrectCvvFormat.toString(), taskEightPage.getStatementAboutInvalidCVVCodeFormat());
    }
}
