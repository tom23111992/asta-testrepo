package steps;

import com.jcraft.jsch.JSchException;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import pages.executors.BasketOneAndSevenPage;
import util.SeleniumExecutor;
import util.helpers.Hooks;
import util.objects.Basket;
import util.objects.BasketItem;

import java.sql.SQLException;

public class CommonSteps {

    SeleniumExecutor executor;
    private BasketOneAndSevenPage basketOneAndSevenPage;
    private Basket basket;
    private BasketItem basketItem;

    public CommonSteps(Basket basket, BasketItem basketItem) {
        executor = SeleniumExecutor.getExecutor();
        this.basketOneAndSevenPage = new BasketOneAndSevenPage(executor);
        this.basket = basket;
        this.basketItem = basketItem;
    }

    @After("@asta")
    public void AfterTests() {

        SeleniumExecutor.stop();
    }

    @After("@task1")
    public void insertDataToDatabase() throws JSchException {

        try {
            Hooks.insertData(basket.getTotalQuantity(), basket.getTotalPrice(), basket);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Given("I go to page \"([^\"]*)\"$")
    public void openBrowser(String page) throws Throwable {

        basketOneAndSevenPage.openPage(page);
    }
}
