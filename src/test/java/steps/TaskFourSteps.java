package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pages.executors.TaskFourPage;
import util.SeleniumExecutor;
import util.enums.DescriptionsMessages;

/**
 * Created by ttutka on 7/13/2016.
 */
public class TaskFourSteps {

    private TaskFourPage taskFourPage;

    public TaskFourSteps() {

        SeleniumExecutor executor = SeleniumExecutor.getExecutor();
        this.taskFourPage = new TaskFourPage(executor);
    }

    @When("^I open form$")
    public void open_form() throws Throwable {

        taskFourPage.openForm();
    }

    @And("^I write ([^\"]*) on name and surname field$")
    public void write_name_and_surname(String text) throws Throwable {

        taskFourPage.fillNameAndSurname(text);
    }

    @And("^I write ([^\"]*) on email field$")
    public void write_email(String text) throws Throwable {

        taskFourPage.fillEmail(text);
    }

    @And("^I write ([^\"]*) on number field$")
    public void write_number(String text) throws Throwable {

        taskFourPage.fillNumber(text);
    }

    @Then("^Should be saved the data$")
    public void save_data() throws Throwable {

        taskFourPage.clickSaveData();
        Assert.assertEquals(DescriptionsMessages.messageSent.toString(),taskFourPage.getFinishMessage() );
    }

    @Then("^Should be information about incorrectly typed fields")
    public void check_fields() throws Throwable {

        taskFourPage.clickSaveData();

        Assert.assertEquals(DescriptionsMessages.invalidEmail.toString(), taskFourPage.getInvalidMail());
        Assert.assertEquals(DescriptionsMessages.invalidPhoneNumber.toString(), taskFourPage.getInvalidPhoneNumber());

    }
}

