package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import pages.executors.BasketOneAndSevenPage;
import util.SeleniumExecutor;
import util.objects.Basket;
import util.objects.BasketItem;

/**
 * Created by ttutka on 7/19/2016.
 */
public class TaskOneSteps {

    private BasketOneAndSevenPage basketOneAndSevenPage;
    private Basket basket;
    private BasketItem basketItem;

    public TaskOneSteps(Basket basket, BasketItem basketItem) {

        SeleniumExecutor executor = SeleniumExecutor.getExecutor();
        this.basketOneAndSevenPage = new BasketOneAndSevenPage(executor);
        this.basket = basket;
        this.basketItem = basketItem;
    }

    public void getCalculatedQuantity(String quantityFromScenario) {
        if (basket.getTotalQuantity() != null) {
            basket.setTotalQuantity(basketOneAndSevenPage.getQuantity(basket.getTotalQuantity(), quantityFromScenario));
        } else basket.setTotalQuantity(quantityFromScenario);
    }

    @And("^I remove item from basket$")
    public void removeItem() throws Throwable {

        basketOneAndSevenPage.removeItemFromBasket();
    }

    @And("^I write \"([^\"]*)\" \"([^\"]*)\" to quantity field and press <Dodaj> on \"([^\"]*)\" item area$")
    public void addItem(int position, String quantity, String name) throws Throwable {

        BasketItem basketItem = new BasketItem();
        basketOneAndSevenPage.typeQuantity(position, quantity);
        basketItem.setName(name);
        basketItem.setPrice(basketOneAndSevenPage.getProductPrice(position));
        basketItem.setQuantity(quantity);

        getCalculatedQuantity(quantity);
        basket.getItems().add(basketItem);

        basketOneAndSevenPage.addToBasketByButton(position);

    }

    @Then("^Should be empty basket$")
    public void checkingBasket() throws Throwable {

        Assert.assertEquals("Quantities are different","0", basketOneAndSevenPage.getTotalQuantityFromBasket());
        Assert.assertEquals("Prices are different","0.0", basketOneAndSevenPage.getTotalPriceFromBasket());
    }

    @Then("^Should be displayed alert$")
    public void checkingDisplayedAlert() throws Throwable {

        basketOneAndSevenPage.checkingAlert();
    }

}





