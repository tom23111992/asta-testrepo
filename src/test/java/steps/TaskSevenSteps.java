package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.unitils.reflectionassert.ReflectionAssert;
import pages.executors.BasketOneAndSevenPage;
import util.SeleniumExecutor;
import util.objects.Basket;
import util.objects.BasketItem;

/**
 * Created by ttutka on 7/19/2016.
 */
public class TaskSevenSteps {

    private BasketOneAndSevenPage basketOneAndSevenPage;
    private Basket basket;
    private BasketItem basketItem;

    public TaskSevenSteps(Basket basket, BasketItem basketItem) {

        SeleniumExecutor executor = SeleniumExecutor.getExecutor();
        this.basketOneAndSevenPage = new BasketOneAndSevenPage(executor);
        this.basket = basket;
        this.basketItem = basketItem;
    }

    public void getCalculatedQuantity(String quantityFromScenario) {
        if (basket.getTotalQuantity() != null) {
            basket.setTotalQuantity(basketOneAndSevenPage.getQuantity(basket.getTotalQuantity(), quantityFromScenario));
        } else basket.setTotalQuantity(quantityFromScenario);

    }

    @And("^I write \"([^\"]*)\" \"([^\"]*)\" to quantity field and drag photo \"([^\"]*)\" item and drop to basket$")
    public void writeToQuantityFieldAndDragPhotoItemAndDropToBasket(int position, String quantity, String name) throws Throwable {

        BasketItem basketItem = new BasketItem();
        basketOneAndSevenPage.typeQuantity(position, quantity);
        basketItem.setName(name);
        basketItem.setPrice(basketOneAndSevenPage.getProductPrice(position));
        basketItem.setQuantity(quantity);
        getCalculatedQuantity(quantity);

        basket.getItems().add(basketItem);
        basketOneAndSevenPage.addToBasketByDragAndDrop(position);
    }

    @And("^I add the same product \"([^\"]*)\" with \"([^\"]*)\" value by button for \"([^\"]*)\"$")
    public void iAddTheSameProductWithValueByButton(int position, String quantity, String name) throws Throwable {

        basketOneAndSevenPage.typeQuantity(position, quantity);


        for (int i = 0; i < basket.getItems().size(); i++) {
            if (basket.getItems().get(i).getName() == name) {

                basket.getItems().get(i).setQuantity(basketOneAndSevenPage.getQuantity(basket.getItems().get(i).getQuantity(), quantity));
                basket.setTotalQuantity(basketOneAndSevenPage.getQuantity(basket.getTotalQuantity(), quantity));
            }
        }
        basketOneAndSevenPage.addToBasketByButton(position);
    }

    @Then("^Should be displayed products on the basket and correctly summary$")
    public void shouldBeDisplayedProductsOnTheBasketAndCorrectlySummary() throws Throwable {

        Basket receivedBasket = new Basket();
        receivedBasket.setItems(basketOneAndSevenPage.getAddedBasketItems());
        receivedBasket.setTotalQuantity(basketOneAndSevenPage.getTotalQuantityFromBasket());
        receivedBasket.setTotalPrice(basketOneAndSevenPage.getTotalPriceFromBasket());

        basket.setTotalPrice(basketOneAndSevenPage.calculatTotalPrice());

        ReflectionAssert.assertReflectionEquals("Product values in basket are incorrect", basket, receivedBasket);
    }

}
