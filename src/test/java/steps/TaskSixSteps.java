package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pages.executors.TaskSixPage;
import util.SeleniumExecutor;
import util.enums.DescriptionsMessages;

/**
 * Created by ttutka on 7/19/2016.
 */
public class TaskSixSteps {

    private TaskSixPage taskSixPage;

    public TaskSixSteps() {
        SeleniumExecutor executor = SeleniumExecutor.getExecutor();
        this.taskSixPage = new TaskSixPage(executor);
    }

    @When("^I write login \"([^\"]*)\"$")
    public void fillName(String text) throws Throwable {

        taskSixPage.fillLogIn(text);
    }

    @And("^I write password \"([^\"]*)\" and press button$")
    public void fillPassword(String text) throws Throwable {

        taskSixPage.fillPassword(text);
        taskSixPage.logInButton();
    }

    @And("^I press download button and log out$")
    public void pressDownload() throws Throwable {

        taskSixPage.downloadButton();
    }

    @When("^I write random login$")
    public void fillNameRandom() throws Throwable {

        taskSixPage.fillLogInRandom();
    }

    @When("^I write random password and press button$")
    public void fillPasswordRandom() throws Throwable {

        taskSixPage.fillPasswordRandom();
        taskSixPage.logInButton();
    }

    @Then("^Should be displayed login form$")
    public void pressLogOut() throws Throwable {

        taskSixPage.logOutButton();
        Assert.assertTrue(taskSixPage.checkIfUserIsLogOut());
    }

    @Then("Should not be logged$")
    public void checkDisplay() throws Throwable {

        Assert.assertEquals(DescriptionsMessages.invalidLogin.toString(), taskSixPage.getInvalidLoginText());
    }
}
