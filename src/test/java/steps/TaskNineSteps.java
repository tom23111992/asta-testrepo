package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pages.executors.TaskNinePage;
import util.SeleniumExecutor;

/**
 * Created by ttutka on 7/21/2016.
 */
public class TaskNineSteps {

    private TaskNinePage taskNinePage;

    public TaskNineSteps() {

        SeleniumExecutor executor = SeleniumExecutor.getExecutor();
        this.taskNinePage = new TaskNinePage(executor);
    }

    @When("I expand tree and changing parent name$")
    public void expand_and_rename_parent() throws Throwable {

        taskNinePage.expandParentAndRename("General");
        Assert.assertEquals(taskNinePage.title, taskNinePage.locators.title.getText());
    }

    @And("I expand child and rename$")
    public void expand_child_and_rename() throws Throwable {

        taskNinePage.expandChildAndRename("First child");
        Assert.assertEquals(taskNinePage.title, taskNinePage.locators.title.getText());
    }

    @And("I rename child name of first child$")
    public void rename_child_first_child() throws Throwable {

        taskNinePage.renameChildFirstChild("Child first child");
        Assert.assertEquals(taskNinePage.title, taskNinePage.locators.title.getText());
    }

    @When("I rename last child name$")
    public void rename_last_child_name() throws Throwable {

        taskNinePage.renameLastChild("Last child");

    }

    @Then("^Should be changed folder names$")
    public void shouldBeChangedFolderNames() throws Throwable {

        Assert.assertEquals(taskNinePage.title, taskNinePage.locators.title.getText());
    }
}
