package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.unitils.reflectionassert.ReflectionAssert;
import pages.executors.TaskThreePage;
import util.SeleniumExecutor;
import util.enums.DescriptionsMessages;
import util.objects.Person;
import util.objects.PersonDetails;

/**
 * Created by ttutka on 7/13/2016.
 */
public class TaskThreeSteps {

    private TaskThreePage taskThreePage;
    private Person person;
    private PersonDetails personDetails = new PersonDetails();

    public TaskThreeSteps(Person person, PersonDetails personDetails) {

        SeleniumExecutor executor = SeleniumExecutor.getExecutor();
        this.taskThreePage = new TaskThreePage(executor);
        this.person = person;
        this.personDetails = personDetails;
    }

    @When("^I turn on edit fields$")
    public void iTryWriteTextToImieField() throws Throwable {

        taskThreePage.checkDisabledField();
        taskThreePage.selectMenu();
    }

    @And("^I write a ([^\"]*) on name field$")
    public void write_name(String text) throws Throwable {

        taskThreePage.enterName(text);
        personDetails.setName(text);
    }

    @And("^I write a ([^\"]*) on surname field$")
    public void write_surname(String text) throws Throwable {

        taskThreePage.enterSurname(text);
        personDetails.setSurname(text);
    }

    @And("^I write a ([^\"]*) on note field$")
    public void write_note(String text) throws Throwable {

        taskThreePage.enterNote(text);
        personDetails.setNotes(text);
    }

    @And("^I write a ([^\"]*) on phone number field$")
    public void write_num(String text) throws Throwable {

        taskThreePage.enterPhoneNumber(text);
        personDetails.setPhoneNumber(text);
    }

    @And("^I add photo ([^\"]*)$")
    public void add_photo(String photo) throws Throwable {

        person.getDetails().add(personDetails);
        taskThreePage.addFile(System.getProperty("user.dir") + photo);
    }

    @And("^I save form$")
    public void saveForm() throws Throwable {

        taskThreePage.saveForm();
    }

    @Then("^Should have displayed alert$")
    public void shouldHaveDisplayedAlert() throws Throwable {

        taskThreePage.checkAlert(DescriptionsMessages.invalidFileFormat.toString());
    }

    @Then("^Should be saved new data$")
    public void shouldBeDisplayedInformationAndShouldBeSavedNewData() throws Throwable {

        Person receivedPerson = new Person();
        receivedPerson.setDetails(taskThreePage.getAddedPersonalDetails());

        ReflectionAssert.assertReflectionEquals(person, receivedPerson);
        Assert.assertTrue(taskThreePage.checkDisplayInformationAboutSaveData());
        Assert.assertEquals(DescriptionsMessages.saveData.toString(), taskThreePage.getInformationAfterSavedForm());
    }
}

