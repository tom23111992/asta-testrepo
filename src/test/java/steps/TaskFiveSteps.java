package steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.executors.TaskFivePage;
import util.SeleniumExecutor;


/**
 * Created by ttutka on 7/18/2016.
 */
public class TaskFiveSteps {

    private TaskFivePage taskFivePage;

    public TaskFiveSteps() {

        SeleniumExecutor executor = SeleniumExecutor.getExecutor();
        this.taskFivePage = new TaskFivePage(executor);
    }

    @When("^I add file \"([^\"]*)\"$")
    public void addFile(String file) throws Throwable {

        taskFivePage.addFile(System.getProperty("user.dir") + "\\files\\" + file);
    }

    @Then("^Should be displayed data from selected file$")
    public void compareData() throws Throwable {

        taskFivePage.assertionData();
    }

    @Then("^Should be displayed text message \"([^\"]*)\"$")
    public void displayedTextMessageAboutQuantityVerses(String alertText) throws Throwable {

        taskFivePage.checkingAlert(alertText);
    }
}
