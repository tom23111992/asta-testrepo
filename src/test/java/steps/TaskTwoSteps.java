package steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pages.executors.TaskTwoPage;
import util.SeleniumExecutor;
import util.enums.DescriptionsMessages;

/**
 * Created by ttutka on 7/13/2016.
 */
public class TaskTwoSteps {

    private TaskTwoPage taskTwoPage;

    public TaskTwoSteps() {

        SeleniumExecutor executor = SeleniumExecutor.getExecutor();
        this.taskTwoPage = new TaskTwoPage(executor);
    }

    @When("^I type on search field part of word \"([^\"]*)\" and choose first position from list$")
    public void writeWord(String text) throws Throwable {

        taskTwoPage.fillSearchFieldCategory(text);
    }

    @When("^I sort products by category \"([^\"]*)\" from dropdown list$")
    public void sortingProducts(String category) throws Throwable {

        taskTwoPage.choosingSortBy(category);
    }

    @When("^I write non exists category")
    public void writeNonExistsCategory() throws Throwable {

        taskTwoPage.typingNoExistsCategory();
    }

    @Then("^Should be products with the category$")
    public void checkingNames() throws Throwable {

        taskTwoPage.checkingSort();
    }

    @Then("^Should be displayed empty list$")
    public void displayInformation() throws Throwable {

        Assert.assertEquals(DescriptionsMessages.noFound.toString(), taskTwoPage.getNoResult());
    }
}
