package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pages.executors.TaskTwoPageSelenide;
import util.enums.DescriptionsMessages;

/**
 * Created by ttutka on 19/01/2017.
 */
public class TaskTwoStepsSelenide {

    private TaskTwoPageSelenide taskTwoPageSelenide;

    public TaskTwoStepsSelenide(TaskTwoPageSelenide taskTwoPageSelenide) {

        this.taskTwoPageSelenide = taskTwoPageSelenide;
    }

    @Given("^Go to page \"([^\"]*)\" selenide$")
    public void iGoToPageSelenide(String url) throws Throwable {

        taskTwoPageSelenide.openPageSelenide(url);
    }

    @When("^I sort products by category \"([^\"]*)\" from dropdown list selenide$")
    public void iSortProductsByCategoryFromDropdownListSelenide(String category) throws Throwable {

        taskTwoPageSelenide.sortBy(category);
    }

    @When("^I type on search field part of word \"([^\"]*)\" and choose first position from list selenide$")
    public void iTypeOnSearchFieldPartOfWordAndChooseFirstPositionFromListSelenide(String text) throws Throwable {

        taskTwoPageSelenide.typeExistsCategory(text);
    }

    @When("^I type no exists category selenide$")
    public void iTypeNoExistsCategory() throws Throwable {

        taskTwoPageSelenide.typeNoExistsCategory();
    }

    @Then("^Should be products with the category selenide$")
    public void shouldBeProductsWithTheCategorySelenide() throws Throwable {

        taskTwoPageSelenide.checkResults();
    }

    @Then("^Should be displayed empty list selenide$")
    public void shouldBeDisplayedEmptyListSelenide() throws Throwable {

        Assert.assertEquals(DescriptionsMessages.noFound.toString(), taskTwoPageSelenide.getNoResult());
    }
}