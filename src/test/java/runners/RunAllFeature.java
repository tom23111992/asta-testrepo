package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"json:target/cucumber.json", "pretty", "html:target/cucumber.html"}, features = "src/test/resources/", glue = "classpath:", tags = {}, dryRun = false)
public class RunAllFeature {
}
