@task3
@asta
Feature: Check the operation menu, validation
  and upload photo by form


  Background:
    Given I go to page "3"

  Scenario Outline: Typing data and sending photo

    When I turn on edit fields
    And I write a <Name> on name field
    And I write a <Surname> on surname field
    And I write a <Notes> on note field
    And I write a <Telephone_number> on phone number field
    And I add photo <File>
    And I save form
    Then Should be saved new data

    Examples:
      | Name | Surname  | Notes      | Telephone_number | File            |
      | Rob  | Mccarthy | thisistest | 666666666        | \\files\\po.jpg |
      | pika | poka     | bulba      | 45642            | \\files\\1.jpg  |


  Scenario Outline: Adding files with other like jpg,png

    When I turn on edit fields
    And I write a <Name> on name field
    And I write a <Surname> on surname field
    And I write a <Notes> on note field
    And I write a <Telephone_number> on phone number field
    And I add photo <File>
    Then Should have displayed alert

    Examples:

      | Name | Surname  | Notes      | Telephone_number | File             |
      | Rob  | Mccarthy | thisistest | 666666666        | \\files\\ppt.ppt |
      | Rob  | Mccarthy | thisistest | 666666666        | \\files\\pdf.pdf |