@task6
Feature: Check operation login and download

  Background:
    Given I go to page "6"

  Scenario Outline: Login and downloading -correct data
    When I write login "<login>"
    And I write password "<password>" and press button
    And I press download button and log out
    Then Should be displayed login form

    Examples:
      | login  | password |
      | tester | 123-xyz  |

  @asta
  Scenario: Login by incorrect data
    When I write random login
    And I write random password and press button
    Then Should not be logged