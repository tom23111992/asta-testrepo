@task8
Feature: Check the operation on the form

  Background:
    Given I go to page "8"


  Scenario Outline: Checking pay cards- correct data

    When I select card type "<Card type>"
    And  I write name "<Name>"
    And  I write card number "<Card number>"
    And  I write CVV code "<CVV code>"
    And  I select month "<Month Card>"
    And  I select year "<Year Card>"
    And  I press pay button
    Then Should be displayed message about correct payment

    Examples:
      | Card type                  | Name     | Card number      | CVV code | Month Card | Year Card |
      | American Express           | Kowalski | 378282246310005  | 005      | August     | 2020      |
      | American Express Corporate | Kowalski | 378734493671000  | 000      | September  | 2022      |
      | Australian BankCard        | Kowalski | 5610591081018250 | 250      | November   | 2018      |
      | Diners Club                | Kowalski | 30569309025904   | 904      | October    | 2017      |
      | Discover                   | Kowalski | 6011111111111117 | 117      | December   | 2017      |


  Scenario Outline: Checking pay cards- incorrect date

    When I select card type "<Card type>"
    And  I write name "<Name>"
    And  I write card number "<Card number>"
    And  I write CVV code "<CVV code>"
    And  I select month "<Month Card>"
    And  I select year "<Year Card>"
    And  I press pay button
    Then Should be displayed information about pay cards pass the expiry date

    Examples:
      | Card type  | Name     | Card number      | CVV code | Month Card | Year Card |
      | JCB        | Kowalski | 3530111333300000 | 000      | January    | 2017      |
      | MasterCard | Kowalski | 5555555555554444 | 444      | January    | 2017      |
      | Visa       | Kowalski | 4012888888881881 | 881      | January    | 2017      |

  @asta
  Scenario Outline: Checking pay cards- incorrect CVV code

    When I select card type "<Card type>"
    And  I write name "<Name>"
    And  I write card number "<Card number>"
    And  I write CVV code "<CVV code>"
    And  I select month "<Month Card>"
    And  I select year "<Year Card>"
    And  I press pay button
    Then Should be displayed information about incorrect format CVV code

    Examples:
      | Card type                | Name     | Card number      | CVV code | Month Card | Year Card |
      | Dankort (PBS)            | Kowalski | 5019717010103742 | 7001     | March      | 2018      |
      | Switch/Solo (Paymentech) | Kowalski | 6331101999990016 | 1111     | April      | 2029      |