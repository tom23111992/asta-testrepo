@asta
@task4
Feature: Type and send a form

  Background:
    Given I go to page "4"

  Scenario Outline: Filling form and sending

    When I open form
    And I write <Name and Surname> on name and surname field
    And I write <Email> on email field
    And I write <Phone_number> on number field
    Then Should be saved the data

    Examples:
      | Name and Surname | Email      | Phone_number |
      | Rob Mccarthy     | test@wp.pl | 666-666-666  |

  Scenario Outline: Checking fields validation

    When I open form
    And I write <Name and Surname> on name and surname field
    And I write <Email> on email field
    And I write <Phone_number> on number field
    Then Should be information about incorrectly typed fields

    Examples:

      | Name and Surname | Email     | Phone_number |
      | Rob Mccarthy     | testwp.pl | 666-666666   |
