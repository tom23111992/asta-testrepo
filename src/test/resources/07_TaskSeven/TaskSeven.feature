@task7
@asta
Feature: Add items to basket - drag&drop

  Background:
    Given I go to page "7"

  Scenario: Adding items to basket by drag&drop

    When I write "0" "10" to quantity field and drag photo "Okulary" item and drop to basket
    And I write "2" "5" to quantity field and drag photo "Kubek" item and drop to basket
    And I write "7" "2" to quantity field and drag photo "Poduszka" item and drop to basket
    And I write "5" "22" to quantity field and drag photo "Kamera" item and drop to basket
    And I write "8" "50" to quantity field and drag photo "Monitor" item and drop to basket
    Then Should be displayed products on the basket and correctly summary

  Scenario: Adding items more than 100 quantity

    When I write "0" "30" to quantity field and drag photo "Okulary" item and drop to basket
    And I write "2" "50" to quantity field and drag photo "Kubek" item and drop to basket
    And I write "7" "22" to quantity field and drag photo "Poduszka" item and drop to basket
    Then Should be displayed alert

  Scenario: Adding the same items to basket by button and drag&drop

    When I write "0" "10" to quantity field and drag photo "Okulary" item and drop to basket
    And I add the same product "0" with "12" value by button for "Okulary"
    And I write "3" "10" to quantity field and drag photo "Kabel" item and drop to basket
    And I add the same product "3" with "12" value by button for "Kabel"
    Then Should be displayed products on the basket and correctly summary

  Scenario: Deleting item from basket

    When I write "0" "10" to quantity field and drag photo "Okulary" item and drop to basket
    And I remove item from basket
    And I write "2" "5" to quantity field and drag photo "Kubek" item and drop to basket
    And I remove item from basket
    Then Should be empty basket
