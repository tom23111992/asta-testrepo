@task2

Feature: Check filtering products on the list

  Background:
    Given I go to page "2"


  Scenario Outline: Selecting each field of dropdown list in sequence and checking sorting

    When I sort products by category "<Category>" from dropdown list
    Then Should be products with the category

    Examples:
      | Category       |
      | Sport          |
      | Elektronika    |
      | Firma i usługi |

  Scenario Outline: Typing on search field category and checking sorting

    When I type on search field part of word "<Word>" and choose first position from list
    Then Should be products with the category

    Examples:

      | Word |
      | Sp   |
      | Ele  |
      | F    |

  @asta
  Scenario: Searching non exists category

    When I write non exists category
    Then Should be displayed empty list