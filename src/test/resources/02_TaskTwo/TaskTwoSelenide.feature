@asta
Feature: Check filtering products on the list - selenide

  Background:
    Given Go to page "2" selenide

  Scenario Outline: Selecting each field of dropdown list in sequence and checking sorting selenide

    When I sort products by category "<Category>" from dropdown list selenide
    Then Should be products with the category selenide

    Examples:
      | Category       |
      | Sport          |
      | Elektronika    |
      | Firma i usługi |

  Scenario Outline: Typing on search field category and checking sorting selenide

    When I type on search field part of word "<Word>" and choose first position from list selenide
    Then Should be products with the category selenide

    Examples:

      | Word |
      | Sp   |
      | Ele  |
      | F    |

  Scenario: Searching non exists category selenide

    When I type no exists category selenide
    Then Should be displayed empty list selenide