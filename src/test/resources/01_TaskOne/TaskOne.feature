@task1
@asta
Feature: Add items to basket

  Background:
    Given I go to page "1"

  Scenario: Adding the same items to basket by button

    When I write "0" "10" to quantity field and press <Dodaj> on "Okulary" item area
    And I add the same product "0" with "12" value by button for "Okulary"
    And I write "2" "20" to quantity field and press <Dodaj> on "Kubek" item area
    And I add the same product "2" with "2" value by button for "Kubek"
    Then Should be displayed products on the basket and correctly summary


  Scenario: Adding items by keyboard

    When I write "0" "5" to quantity field and press <Dodaj> on "Okulary" item area
    And I write "2" "5" to quantity field and press <Dodaj> on "Kubek" item area
    And I write "5" "22" to quantity field and press <Dodaj> on "Kamera" item area
    And I write "7" "2" to quantity field and press <Dodaj> on "Poduszka" item area
    And I write "8" "50" to quantity field and press <Dodaj> on "Monitor" item area
    Then Should be displayed products on the basket and correctly summary

  Scenario: Adding items more than 100

    And I write "0" "30" to quantity field and press <Dodaj> on "Okularyy" item area
    And I write "1" "50" to quantity field and press <Dodaj> on "Pilkaa" item area
    And I write "2" "22" to quantity field and press <Dodaj> on "Kubekk" item area
    Then Should be displayed alert

  Scenario: Deleting items from basket

    And I write "0" "10" to quantity field and press <Dodaj> on "Okulary" item area
    And I write "2" "5" to quantity field and press <Dodaj> on "Kubek" item area
    And I remove item from basket
    And I remove item from basket
    Then Should be empty basket
