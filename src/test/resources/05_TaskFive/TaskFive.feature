@task5
Feature: Send file and check data

  Background:
    Given I go to page "5"

  Scenario: Adding file with a quantity verses less than 20

    When I add file "dane.txt"
    Then Should be displayed data from selected file

  @asta
  Scenario Outline: Adding file with incorrect amount of columns

    When I add file "<File>"
    Then Should be displayed text message "<Text>"

    Examples:

      | File                            | Text                                                                     |
      | dane_more_than_20.txt           | Maksymalnie wprowadzić można 20 wierszy.                                 |
      | dane_number_has_more_than_9.txt | Numer telefonu może zawierać tylko znaki numeryczne, musi mieć 9 znaków. |
      | 1.jpg                           | Zły format pliku                                                         |
      | columns.txt                     | Źle sformatowany plik.                                                   |